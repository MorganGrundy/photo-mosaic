#pragma once

#define VERSION_MAJOR 3
#define VERSION_MINOR 4
#define VERSION_BUILD 2

#define VERSION_STR "v3.4.2"
